#!/usr/bin/python2

import argparse
import json
import sys
import SocketServer
import os
import threading
import time
import urllib
import urlparse
import signal
import cStringIO

if sys.version_info[0] == 2:
    import SimpleHTTPServer
elif sys.version_info[0] == 3:
    pass


script_dir = os.path.realpath(os.path.dirname(__file__))
index_html = os.path.join(script_dir, 'index.html')
with open(index_html, 'rb') as f:
    index_html_data = f.read()
    index_html_fstat = os.fstat(f.fileno())
    index_html_last_modified = str(index_html_fstat.st_mtime)


class _DataManger(object):

    def __init__(self):
        self.count = 0
        self.columns = ['id', 'value']
        self.rows = [
            [1, 0],
            [2, 0]
        ]

    def get_table_data(self):
        self.count += 1
        self.rows[0] = [1, self.count]
        self.rows[1] = [2, self.count - 1]
        return {
            'columns': self.columns,
            'rows': self.rows
        }

    def get_value_data(self, index):
        value = ''
        row = self.rows[index]
        for i in range(len(self.columns)):
            value += '<' + str(self.columns[i]) + '>'
            value += str(row[i])
            value += '</' + str(self.columns[i]) + '>'
        return '<value>' + value + '</value>'

    def clear(self):
        self.count = 0
        return '{}'


DataManger = _DataManger()


class HttpHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def send_head(self):
        """Common code for GET and HEAD commands.

        This sends the response code and MIME headers.

        Return value is either a file object (which has to be copied
        to the outputfile by the caller unless the command was HEAD,
        and must be closed by the caller under all circumstances), or
        None, in which case the caller has nothing further to do.

        """
        parts = urlparse.urlsplit(self.path)
        path = parts[2]
        query = parts[3]
        content_length = 0
        last_modified = int(time.time())
        f = None
        if path == '/table' or path == '/table/':
            ctype = 'application/json'
            data = json.dumps(DataManger.get_table_data())
        elif path == '/value' or path == '/value/':
            query = urlparse.parse_qs(query)
            data_id = '-1'
            if 'id' in query:
                data_id = query['id'][0]
            ctype = 'application/xml'
            data = DataManger.get_value_data(data_id)
        elif path == '/clear' or path == '/clear/':
            ctype = 'application/json'
            data = DataManger.clear()
        elif path == '/' or path == '/index.html':
            ctype = 'text/html'
            data = index_html_data
            last_modified = index_html_last_modified
        else:
            self.send_error(404, "File not found")
            return None
        try:
            f = cStringIO.StringIO(data)
            content_length = len(data)
            self.send_response(200)
            self.send_header("Content-type", ctype)
            self.send_header("Content-Length", str(content_length))
            self.send_header("Last-Modified", self.date_time_string(last_modified))
            self.end_headers()
            return f
        except:
            f.close()
            raise

    def do_HEAD(self):
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_HEAD(self)

    def do_GET(self):
        SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('SimpleDataMonitor')
    parser.add_argument('-p', '--port', dest='port', type=int, default=8000, help='port number')
    args = parser.parse_args()
    port = args.port
    httpd = SocketServer.TCPServer(("", args.port), HttpHandler)
    print "serving at port - {}".format(port)

    def handler(signum, frame):
        stop_thread = threading.Thread(target=httpd.shutdown)
        stop_thread.start()
        print('Server shutdown')

    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)

    httpd.serve_forever()
